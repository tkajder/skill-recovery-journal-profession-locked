require "TimedActions/ISReadABook"

local SRJ_ISReadABook_update;
local SRJPL_ISReadABook_update = function(self)
	if self.item and self.item:getType() == "SkillRecoveryJournal" and self.character then
        local journalModData = self.item:getModData();
        journalModData["SRJ"] = journalModData["SRJ"] or {};
        local JMD = journalModData["SRJ"];
        JMD["ID"] = JMD["ID"] or {};
        local JMD_ID = JMD["ID"];

        local playerSteamID = self.character:getSteamID();
        local journalSteamID = JMD_ID["steamID"]
        local playerProfession = self.character:getDescriptor():getProfession();

        -- if we are multiplayer and the journal steam id does not match the player steam id, defer to SRJ error checking and exit
        if playerSteamID ~= 0 and journalSteamID and journalSteamID ~= playerSteamID then
            SRJ_ISReadABook_update(self);
            return;
        end

        if JMD["profession"] ~= nil and JMD["profession"] == playerProfession then
            SRJ_ISReadABook_update(self);
        elseif JMD["profession"] ~= nil and JMD["profession"] ~= playerProfession then
            local errorText = getText("IGUI_PlayerText_DoesntFeelRightToRead");
            self.character:Say(errorText);
            self:forceStop();
        else
            -- since journal and player steam id "match" we are safe to set the journal mod data for an unset journal 
            JMD["profession"] = playerProfession;
            SRJ_ISReadABook_update(self);
        end
    else
        SRJ_ISReadABook_update(self);
    end
end

Events.OnGameStart.Add(
    function()
        SRJ_ISReadABook_update = ISReadABook.update;
        ISReadABook.update = SRJPL_ISReadABook_update;
    end
)