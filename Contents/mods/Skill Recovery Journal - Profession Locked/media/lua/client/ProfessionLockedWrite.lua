require "TimedActions/ISCraftAction"

local SRJ_ISCraftAction_new;
local SRJPL_ISCraftAction_new = function(self, character, item, time, recipe, container, containers)
    local o = SRJ_ISCraftAction_new(self, character, item, time, recipe, container, containers);

    if o ~= nil and character then
        local journalModData = item:getModData();
        journalModData["SRJ"] = journalModData["SRJ"] or {};
        local JMD = journalModData["SRJ"];

        local playerProfession = character:getDescriptor():getProfession();

        JMD["profession"] = playerProfession;
    end

    return o
end

local SRJ_ISCraftAction_update;
local SRJPL_ISCraftAction_update = function(self)
    if self.recipe and self.recipe:getOriginalname() == "Transcribe Journal" and self.item and self.item:getType() == "SkillRecoveryJournal" and self.character then
        local journalModData = self.item:getModData();
        journalModData["SRJ"] = journalModData["SRJ"] or {};
        local JMD = journalModData["SRJ"];
        JMD["ID"] = JMD["ID"] or {};
        local JMD_ID = JMD["ID"];
        
        local playerSteamID = self.character:getSteamID();
        local journalSteamID = JMD_ID["steamID"]
        local playerProfession = self.character:getDescriptor():getProfession();

        -- if we are multiplayer and the journal steam id does not match the player steam id, defer to SRJ error checking and exit
        if playerSteamID ~= 0 and journalSteamID and journalSteamID ~= playerSteamID then
            SRJ_ISCraftAction_update(self);
            return;
        end

        if JMD["profession"] and JMD["profession"] == playerProfession then
            SRJ_ISCraftAction_update(self);
        elseif JMD["profession"] ~= nil and JMD["profession"] ~= playerProfession then
            local errorText = getText("IGUI_PlayerText_DoesntFeelRightToWrite");
            self.character:Say(errorText);
        else
            -- since journal and player steam id "match" we are safe to set the journal mod data for an unset journal 
            JMD["profession"] = playerProfession;
            SRJ_ISCraftAction_update(self);
        end
    else
        SRJ_ISCraftAction_update(self);
    end
end

-- TODO: I could not seem to grab/overwrite `ISCraftAction.new` the same way that SKill Recovery Journal did.
-- TODO: This may lead to a condition where this mod grabs the "wrong" `ISCraftAction.new` and fails to execute properly
Events.OnGameStart.Add(
    function()
        SRJ_ISCraftAction_new = ISCraftAction.new;
        ISCraftAction.new = SRJPL_ISCraftAction_new;
        SRJ_ISCraftAction_update = ISCraftAction.update;
        ISCraftAction.update = SRJPL_ISCraftAction_update;
    end
)